
%{
open Action;;

let max n m = if n > m then n else m

let rec log2 n = if n = 0 then -1 else 1 + log2 (n / 2)

 let size = ref 0
 let size s =
   let n = log2 s in
   if 1 lsl n = s then n else n + 1

%}

/* Tokens */
%token <int> X
%token <int> Y
%token <int> ON
%token <int> OFF
%token <int> NEWLINE
%token <string*string>RULE
%token EOD
%token EOF

%start file

%type <(int * int * int) * Action.action list> file

%%

file: h=header; d=desc; EOD EOF { (h, List.rev d) };

header: x=X; y=Y; RULE { x, y, size (max x y) };

desc:
| d=desc; s=step; { s :: d }
| s=step; { [s] }
;

step:
| o=ON; { On o }
| o=OFF; { Off o }
| i=NEWLINE; { Newline i }
;
