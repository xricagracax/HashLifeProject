
(* The type of tokens. *)

type token = 
  | Y of (int)
  | X of (int)
  | RULE of (string*string)
  | ON of (int)
  | OFF of (int)
  | NEWLINE of (int)
  | EOF
  | EOD

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val file: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> ((int * int * int) * Action.action list)
