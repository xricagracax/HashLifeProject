{
  open Parser
  exception Error of string
}

let digit = ['0'-'9']
let int = (digit+)
let space = [' ' '\t' '\n']*
let comment = '#' [^'\n']* '\n'

rule token = parse
  | comment { token lexbuf }
  | ['x' 'X'] space '=' space (int as x) space ',' { X(int_of_string x) }
  | ['y' 'Y'] space '=' space (int as y) space ',' { Y(int_of_string y) }
  | "rule" space '=' space 'B' (int as ruleb) '/' 'S' (int as rules) { RULE(ruleb,rules) }
  | (int as n) 'o' { ON(int_of_string n) }
  | (int as n) 'b' { OFF(int_of_string n) }
  | (int as n) '$' { NEWLINE(int_of_string n) }
  | 'o' { ON(1) }
  | 'b' { OFF(1) }
  | '$' { NEWLINE(1) }
  | '!' { EOD }
  | space  { token lexbuf }
  | eof  { EOF }
  | _ { raise (Error (Printf.sprintf "At offset %d: unexpected character.\n" (Lexing.lexeme_start lexbuf))) }
