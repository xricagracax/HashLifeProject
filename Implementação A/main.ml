(*Biblioteca para mostrar os gráficos*)
open Graphics;;
open Printf;;
open Hashlife;;
open Unix;;
open Scanf;;
open Array;;
open Printf;;
open List;;
open Action;;
open Parser;;
open Str;;
open Queue;;
open Big_int_Z;;

type coord = 
|NW|N|NE
|W |C| E
|SW|S|SE

let size = 1024;;
let dx = 2;; 
let font = "*--0-0-*iso8859-*";;
let font_size = 20;;

(*Número de células*)
let number_of_cells = ref Z.zero;;
let number_of_cells_created = ref 0;;
let number_of_existing_cells = ref 0;;

(*Colors*)
let color_alive = black;;
let color_dead = white;;

let myQ = Queue.create();;

(*Ir para dentro*)
let myFunction ti p = 

	match ti with 
	| Cell(a,b,c,d) -> raise (Failure "Can't zoom more than this!")
	| MacroCell((nw,ne,sw,se),_,_,_) -> 
	match nw.node, ne.node, sw.node, se.node with
	| MacroCell((nw1,ne1,sw1,se1),_,_,_),MacroCell((nw2,ne2,sw2,se2),_,_,_),MacroCell((nw3,ne3,sw3,se3),_,_,_),MacroCell((nw4,ne4,sw4,se4),_,_,_)
	 -> (match p with 
		 | N ->  macrocell(ne1,nw2,se1,sw2)
		 | S ->  macrocell(ne3,nw4,se3,sw4)
		 | E ->  macrocell(sw2,se2,nw4,ne4)
		 | W ->  macrocell(sw1,se1,nw3,ne3)
		 | C ->  macrocell(se1,sw2,ne3,nw4)
		 | NW -> macrocell(nw1,ne1,sw1,se1)
		 | NE -> macrocell(nw2,ne2,sw2,se2)
		 | SW -> macrocell(nw3,ne3,sw3,se3)
		 | SE -> macrocell(nw4,ne4,sw4,se4)
		)
	 | Cell(nw1,ne1,sw1,se1),Cell(nw2,ne2,sw2,se2),Cell(nw3,ne3,sw3,se3),Cell(nw4,ne4,sw4,se4)
	 ->
	 	(match p with 
		 | N ->  cell(ne1,nw2,se1,sw2)
		 | S ->  cell(ne3,nw4,se3,sw4)
		 | E ->  cell(sw2,se2,nw4,ne4)
		 | W ->  cell(sw1,se1,nw3,ne3)
		 | C ->  cell(se1,sw2,ne3,nw4)
		 | NW -> cell(nw1,ne1,sw1,se1)
		 | NE -> cell(nw2,ne2,sw2,se2)
		 | SW -> cell(nw3,ne3,sw3,se3)
		 | SE -> cell(nw4,ne4,sw4,se4)
		)

	 |_ -> raise (Failure "Escolha uma coordenada")

(*Desenhar árvore*)
let draw_tree t = 
	(*set_color color_dead; fill_rect (x_i) (y_i) (size) (size);*)
	let cp = Queue.copy myQ in 
	let x = size in 
	let y = size in 
	let draw_cell bl n a b = 
	match bl with 
	| true  -> set_color color_alive; fill_rect (a) ( b) (x/n) (y/n)
	| _     -> set_color color_dead; fill_rect (a) (b) (x/n) (y/n) in
	let rec draw_tree_aux ti n a b = 
		if Queue.is_empty cp then (
			let new_n = n*2 in 
			match ti with 
			| Cell(nw,ne,sw,se) -> begin
				draw_cell nw (new_n) (    a    ) (b+y/new_n);
				draw_cell ne (new_n) (a+x/new_n) (b+y/new_n);
				draw_cell sw (new_n) (    a    ) (    b    );
				draw_cell se (new_n) (a+x/new_n) (    b    );
				end
			| MacroCell((nw,ne,sw,se),_,_,_) -> begin
				if empty_macrocell ti then (
						set_color color_dead; fill_rect (a) (b) (size/n) (size/n);
					)
				else (
					if full_macrocell ti then (
						set_color color_alive; fill_rect (a) (b) (size/n) (size/n);
					)
					else(
						draw_tree_aux nw.node (new_n) (    a    ) (b+y/new_n);
						draw_tree_aux ne.node (new_n) (a+x/new_n) (b+y/new_n);
						draw_tree_aux sw.node (new_n) (    a    ) (    b    );
						draw_tree_aux se.node (new_n) (a+x/new_n) (    b    );
					)
				)
				end
		)
		else(
			let aux_ = myFunction ti (Queue.pop cp) in 
			draw_tree_aux (aux_.node) n a b 
		)
	in draw_tree_aux (t) 1 0 0

(*Desenhar árvore mais rápida*)
let draw_tree_faster ta tb =

	if dimension ta != dimension tb then (
		draw_tree ta.node
	)
	else(
		(*set_color color_dead; fill_rect (x_i) (y_i) (size) (size);*)
		let x = size in 
		let y = size in 
		let cp = Queue.copy myQ in 
		let draw_cell bl n a b = 
		match bl with 
		| true  -> set_color color_alive; fill_rect (a) (b) (x/n) (y/n)
		| _     -> set_color color_dead; fill_rect (a) (b) (x/n) (y/n) in
		let rec draw_tree_aux tai tbi n a b = 
		if Queue.is_empty cp then (
			let new_n = n*2 in 
			match tai,tbi with 
			| Cell(nw,ne,sw,se),Cell(_,_,_,_) -> begin
				draw_cell nw (new_n) (    a    ) (b+y/new_n);
				draw_cell ne (new_n) (a+x/new_n) (b+y/new_n);
				draw_cell sw (new_n) (    a    ) (    b    );
				draw_cell se (new_n) (a+x/new_n) (    b    );
				end
			| MacroCell((nw1,ne1,sw1,se1),_,_,_), MacroCell((nw2,ne2,sw2,se2),_,_,_) -> begin
				if empty_macrocell tai then (
					set_color color_dead; fill_rect (a) (b) (size/n) (size/n);
				)
				else (
				if full_macrocell tai then (
					set_color color_alive; fill_rect (a) (b) (size/n) (size/n);
				)
				else(
					if nw1 != nw2 then (
						draw_tree_aux nw1.node nw2.node (new_n) (    a    ) (b+y/new_n)
					);
					if ne1 != ne2 then (
						draw_tree_aux ne1.node ne2.node (new_n) (a+x/new_n) (b+y/new_n)
					);
					if sw1 != sw2 then (
						draw_tree_aux sw1.node sw2.node (new_n) (    a    ) (    b    )
					);
					if se1 != se2 then (
						draw_tree_aux se1.node se2.node (new_n) (a+x/new_n) (    b    )
					);
				)
			)
			end
			| _ -> raise (Failure "Erro... Diferentes")
		)
		else(
			let e = Queue.pop cp in 
			let aux_a = myFunction tai (e) in 
			let aux_b = myFunction tbi (e) in 
			draw_tree_aux aux_a.node aux_b.node n a b 
		)
		in draw_tree_aux (ta.node) (tb.node) 1 0 0
	)

(*Desenhar árvore*)
let draw_matrix m = 

	set_color white; fill_rect (0) (0) (size) (size);
	let x = size in 
	let y = size in 
	let size = Array.length m in 
	
	let draw_cell bl i j = 
	match bl with 
	| true  -> set_color color_alive; fill_rect ((j)*(x/size)) ((size-1-i)*(y/size)) (x/size) (y/size)
	| _     -> set_color color_dead; fill_rect ((j)*(x/size)) ((size-1-i)*(y/size)) (x/size) (y/size) in
	
	for i=0 to (size-1) do 
	for j=0 to (size-1) do 
	draw_cell m.(i).(j) i j
	done;
	done

let print_matrix m = 
	let size = Array.length m in 
	for i=0 to (size-1) do 
	for j=0 to (size-1) do
	if m.(i).(j) then 
	printf "X"
	else printf " "
	done; print_newline ();
	done

let file_to_matrix filename = 

	let inBuffer = open_in filename in
	let lineBuffer = Lexing.from_channel inBuffer in
	try
	let (x1,y1,a),b = Parser.file Lexer.token lineBuffer in
	let read l s = 
		let size = 1 lsl s in 
		let matrix = Array.init size (fun e -> Array.make size false) in 
		let y2,x2 = (size-y1)/2,(size-x1)/2 in 
			let rec read_aux li i j = (
			match li with
			| [] -> ()
			| hd :: tl -> (
			match hd with 
			| On el -> (
				for t=0 to (el-1) do 
				matrix.(i).(j+t) <- true;
				done;
				read_aux tl (i) (j+el)
			)
				| Off el -> read_aux tl (i) (j+el)
				| Newline el -> read_aux tl (i+el) (x2)
			)
			)
			in read_aux l y2 x2; matrix;
	in read b a;
	with
	| Lexer.Error msg -> [|[||]|]
	| Parser.Error -> [|[||]|]

;;

let string_to_coord str = 
	match str with 
	|"NW" -> NW
	|"N" -> N
	|"NE" -> NE
	|"W" -> W 
	|"C" -> C
	|"E" -> E
	|"SW" -> SW
	|"S" -> S
	|"SE" -> SE
	| _ -> raise (Failure "Escolha uma coordenada")

let coord_to_str coord = 
	match coord with 
	|NW -> "NW"
	|N -> "N"
	|NE -> "NE"
	|W -> "W"
	|C -> "C"
	|E -> "E"
	|SW -> "SW"
	|S -> "S"
	|SE -> "SE"

(*Load*)
let tree = ref(cell (false,false,false,false))

(*Animation*)
let interval = ref Z.one
let frames = ref Z.one 
let timeperframe = ref 0.001
let save = ref false

(*Main program 4*)
let main_1 () = 
	let (a,b,c,d,e,f),g = stats() in 
	printf "Number of macro-cells created: %s\n" (Big_int_Z.string_of_big_int g);
	printf "Number of cells alive in the current macro-cell: %s\n" (Big_int_Z.string_of_big_int (number_cells (!tree)));
	printf "Dimension of the current macro-cell: %d\n" (dimension (!tree))

(*Main program 2*)
let main_2 t interval frames = 
	clear_graph ();
	let tree_aux = ref( t ) in
	let i = ref Z.zero in 
	while Big_int_Z.lt_big_int !i frames do 
		let aux = (!tree_aux) in 
		tree_aux :=  reduce_macrocell (tree_result_generalized_up (extend_macrocell (extend_macrocell (!tree_aux))) interval);
		sleepf(!timeperframe);
		draw_tree_faster (!tree_aux) aux;
		i := Big_int_Z.add_int_big_int 1 (!i)
	done;
	if !save then (
		tree := !tree_aux
	);
	save := false

(*Main program 4*)
let main_4 tree generation = 
	let start = Unix.gettimeofday () in
	let result = (tree_result_generalized_up (tree) generation) in
	(* let result = reduce_macrocell (tree_result_generalized_up (extend_macrocell (extend_macrocell tree)) generation) in *)
	(*let result = tree_result_generalized_up tree_aux (Big_int_Z.big_int_of_string "6366548773467669985195496000") in*)
	(*let result = tree_result_generalized_up tree_aux (Big_int_Z.power_int_positive_int 10 100) in *)
	let stop = Unix.gettimeofday () in 
	printf "Execution time: %fs\n%!" (stop -. start);
	result

(*Comandos*)
let usage_load = "usage: load [-f filename] [-r int]"
let usage_show = "usage: show "
let usage_animation = "usage: animation [-s save] [-t interval] [-n frames] [-tf time per frame] "
let usage_result = "usage: result [-n generation] "
let usage_zoom = "usage: zoom [-out] [-in coord]"
let usage_info = "usage: info"
let usage_exit = "usage: exit"

let speclist_load = [
		("-r", Arg.Int (fun d -> tree := random_tree d; printf "SUCCESS! RANDOM GENERATED\n" ), ": -r sets the dimension for a random generation");
		("-f", Arg.String (fun d ->
			let file_name = d in 
			let matrix = file_to_matrix file_name in 
			tree := (matrix_to_tree matrix);
			printf "SUCCESS! TREE CREATED\n"
			), ": -f sets the filename");
	]

let speclist_show = [
	("-r", Arg.Int (fun d -> tree := random_tree d ), ": -r sets the dimension for a random generation");
]

let speclist_animation = [
	("-n" , Arg.String (fun d -> frames := Big_int_Z.big_int_of_string d), ": -n sets the number of frames");
	("-t" , Arg.String (fun d -> interval := Big_int_Z.big_int_of_string d), ": -t sets the interval between frames");
	("-tf", Arg.Float  (fun d -> timeperframe := d), ": -t sets the time per frame");
	("-s", Arg.Unit  (fun s -> save := true), ": -s save animation");
]

let speclist_result = [
	("-n", Arg.String (fun d -> tree := reduce_macrocell (main_4 (!tree) (Big_int_Z.big_int_of_string  d))), ": -n sets the generation number");
]

let speclist_info = [
]

let speclist_zoom = [
	("-out" , Arg.Unit (fun _ -> let _ = Queue.pop myQ; in (printf "ZOOMED OUT\n";print_newline())),  ": -out, zooms out");
	("-in" , Arg.String (fun s -> Queue.push (string_to_coord s) myQ), ": -in coord, zooms in");]

let parseinput userinp =
	(* Read the arguments *)
	let usage,speclist = (
		match userinp.(0) with 
		| "load" -> usage_load, speclist_load
		| "show" -> usage_show,speclist_show
		| "animation" -> usage_animation,speclist_animation
		| "result" -> usage_result,speclist_result
		| "zoom" -> usage_zoom,speclist_zoom
		| "info" -> usage_info,speclist_info
		| "exit" -> exit(0);
		| _ -> raise (Arg.Bad ("That command doesn't exist\n"^usage_load^"\n"^usage_show^"\n"^usage_animation^"\n"^usage_result^"\n"^usage_zoom^"\n"^usage_exit^"\n"))
	) in 
	Arg.parse_argv ?current:(Some (ref 0)) userinp
		speclist
		(fun x -> raise (Arg.Bad ("Bad argument : " ^ x)))
		usage;

match userinp.(0) with 
| "show" -> draw_tree (!tree).node
| "animation" -> main_2 ((!tree)) (!interval) (!frames); timeperframe := 0.0; interval := Z.one; frames := Z.one 
| "zoom" -> (Queue.iter (fun e -> printf "%s" (coord_to_str e);) myQ); print_newline ();
| "info" -> main_1 ()
| _ -> ()

let  parseit line =
	(* TODO rewrite without Str*)
	let listl = (Str.split (Str.regexp " ") line) in
	parseinput (Array.of_list listl)

(* Ponto de entrada do programa principal *)
let _ = 
	let size_in_string = string_of_int (size) in 
	Graphics.open_graph (" "^size_in_string^"x"^size_in_string);
	Graphics.set_font font;
	set_window_title "Conway's Game of Life";
(* 	let size = 8 in 
	main_4 (random_tree size) (shift_left_big_int Z.one (size-2));
	exit(0); *)
	while true do
	try
			printf "HashLife>";
			flush (Pervasives.stdout);
			let line = input_line Pervasives.stdin in 
			parseit line
	with
		|End_of_file -> ();
		|Invalid_argument arg -> (printf "%s" arg)
		|Arg.Bad m -> printf "%s" m
		|Arg.Help m -> printf "%s" m 
		|Queue.Empty -> printf "Already zoomed as far as it can go!"
		|Sys_error m -> printf "That file does not exist\n"
		|Failure m -> printf "%s\n" (m)
		|Division_by_zero -> printf "This configuration is too big to be shown\n"
		|Stack_overflow -> printf "File couldn't be parsed!"
	done
