
module MenhirBasics = struct
  
  exception Error
  
  type token = 
    | Y of (
# 18 "parser.mly"
       (int)
# 11 "parser.ml"
  )
    | X of (
# 17 "parser.mly"
       (int)
# 16 "parser.ml"
  )
    | RULE of (
# 22 "parser.mly"
       (string*string)
# 21 "parser.ml"
  )
    | ON of (
# 19 "parser.mly"
       (int)
# 26 "parser.ml"
  )
    | OFF of (
# 20 "parser.mly"
       (int)
# 31 "parser.ml"
  )
    | NEWLINE of (
# 21 "parser.mly"
       (int)
# 36 "parser.ml"
  )
    | EOF
    | EOD
  
end

include MenhirBasics

let _eRR =
  MenhirBasics.Error

type _menhir_env = {
  _menhir_lexer: Lexing.lexbuf -> token;
  _menhir_lexbuf: Lexing.lexbuf;
  _menhir_token: token;
  mutable _menhir_error: bool
}

and _menhir_state = 
  | MenhirState9
  | MenhirState4

# 2 "parser.mly"
  
open Action;;

let max n m = if n > m then n else m

let rec log2 n = if n = 0 then -1 else 1 + log2 (n / 2)

 let size = ref 0
 let size s =
   let n = log2 s in
   if 1 lsl n = s then n else n + 1


# 73 "parser.ml"

let rec _menhir_goto_desc : _menhir_env -> 'ttv_tail -> _menhir_state -> (Action.action list) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_stack = (_menhir_stack, _menhir_s, _v) in
    let _menhir_stack = Obj.magic _menhir_stack in
    assert (not _menhir_env._menhir_error);
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | EOD ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_s = MenhirState9 in
        let _menhir_stack = (_menhir_stack, _menhir_s) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | EOF ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (((_menhir_stack, (h : (int * int * int))), _, (d : (Action.action list))), _) = _menhir_stack in
            let _4 = () in
            let _3 = () in
            let _v : (
# 28 "parser.mly"
      ((int * int * int) * Action.action list)
# 98 "parser.ml"
            ) = 
# 32 "parser.mly"
                                ( (h, List.rev d) )
# 102 "parser.ml"
             in
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_1 : (
# 28 "parser.mly"
      ((int * int * int) * Action.action list)
# 109 "parser.ml"
            )) = _v in
            Obj.magic _1
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            let (_menhir_stack, _menhir_s) = _menhir_stack in
            _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s)
    | NEWLINE _v ->
        _menhir_run7 _menhir_env (Obj.magic _menhir_stack) MenhirState9 _v
    | OFF _v ->
        _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState9 _v
    | ON _v ->
        _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState9 _v
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState9

and _menhir_goto_step : _menhir_env -> 'ttv_tail -> _menhir_state -> (Action.action) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    match _menhir_s with
    | MenhirState4 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (s : (Action.action)) = _v in
        let _v : (Action.action list) = 
# 38 "parser.mly"
          ( [s] )
# 139 "parser.ml"
         in
        _menhir_goto_desc _menhir_env _menhir_stack _menhir_s _v
    | MenhirState9 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = Obj.magic _menhir_stack in
        let (s : (Action.action)) = _v in
        let (_menhir_stack, _menhir_s, (d : (Action.action list))) = _menhir_stack in
        let _v : (Action.action list) = 
# 37 "parser.mly"
                  ( s :: d )
# 150 "parser.ml"
         in
        _menhir_goto_desc _menhir_env _menhir_stack _menhir_s _v

and _menhir_errorcase : _menhir_env -> 'ttv_tail -> _menhir_state -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s ->
    match _menhir_s with
    | MenhirState9 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let (_menhir_stack, _menhir_s, _) = _menhir_stack in
        _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) _menhir_s
    | MenhirState4 ->
        let _menhir_stack = Obj.magic _menhir_stack in
        raise _eRR

and _menhir_run5 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 19 "parser.mly"
       (int)
# 168 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (o : (
# 19 "parser.mly"
       (int)
# 176 "parser.ml"
    )) = _v in
    let _v : (Action.action) = 
# 42 "parser.mly"
        ( On o )
# 181 "parser.ml"
     in
    _menhir_goto_step _menhir_env _menhir_stack _menhir_s _v

and _menhir_run6 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 20 "parser.mly"
       (int)
# 188 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (o : (
# 20 "parser.mly"
       (int)
# 196 "parser.ml"
    )) = _v in
    let _v : (Action.action) = 
# 43 "parser.mly"
         ( Off o )
# 201 "parser.ml"
     in
    _menhir_goto_step _menhir_env _menhir_stack _menhir_s _v

and _menhir_run7 : _menhir_env -> 'ttv_tail -> _menhir_state -> (
# 21 "parser.mly"
       (int)
# 208 "parser.ml"
) -> 'ttv_return =
  fun _menhir_env _menhir_stack _menhir_s _v ->
    let _menhir_env = _menhir_discard _menhir_env in
    let _menhir_stack = Obj.magic _menhir_stack in
    let (i : (
# 21 "parser.mly"
       (int)
# 216 "parser.ml"
    )) = _v in
    let _v : (Action.action) = 
# 44 "parser.mly"
             ( Newline i )
# 221 "parser.ml"
     in
    _menhir_goto_step _menhir_env _menhir_stack _menhir_s _v

and _menhir_discard : _menhir_env -> _menhir_env =
  fun _menhir_env ->
    let lexer = _menhir_env._menhir_lexer in
    let lexbuf = _menhir_env._menhir_lexbuf in
    let _tok = lexer lexbuf in
    {
      _menhir_lexer = lexer;
      _menhir_lexbuf = lexbuf;
      _menhir_token = _tok;
      _menhir_error = false;
    }

and file : (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (
# 28 "parser.mly"
      ((int * int * int) * Action.action list)
# 240 "parser.ml"
) =
  fun lexer lexbuf ->
    let _menhir_env = let _tok = Obj.magic () in
    {
      _menhir_lexer = lexer;
      _menhir_lexbuf = lexbuf;
      _menhir_token = _tok;
      _menhir_error = false;
    } in
    Obj.magic (let _menhir_stack = ((), _menhir_env._menhir_lexbuf.Lexing.lex_curr_p) in
    let _menhir_env = _menhir_discard _menhir_env in
    let _tok = _menhir_env._menhir_token in
    match _tok with
    | X _v ->
        let _menhir_stack = Obj.magic _menhir_stack in
        let _menhir_stack = (_menhir_stack, _v) in
        let _menhir_env = _menhir_discard _menhir_env in
        let _tok = _menhir_env._menhir_token in
        (match _tok with
        | Y _v ->
            let _menhir_stack = Obj.magic _menhir_stack in
            let _menhir_stack = (_menhir_stack, _v) in
            let _menhir_env = _menhir_discard _menhir_env in
            let _tok = _menhir_env._menhir_token in
            (match _tok with
            | RULE _v ->
                let _menhir_stack = Obj.magic _menhir_stack in
                let _menhir_env = _menhir_discard _menhir_env in
                let _menhir_stack = Obj.magic _menhir_stack in
                let (_3 : (
# 22 "parser.mly"
       (string*string)
# 273 "parser.ml"
                )) = _v in
                let ((_menhir_stack, (x : (
# 17 "parser.mly"
       (int)
# 278 "parser.ml"
                ))), (y : (
# 18 "parser.mly"
       (int)
# 282 "parser.ml"
                ))) = _menhir_stack in
                let _v : (int * int * int) = 
# 34 "parser.mly"
                       ( x, y, size (max x y) )
# 287 "parser.ml"
                 in
                let _menhir_stack = (_menhir_stack, _v) in
                let _menhir_stack = Obj.magic _menhir_stack in
                assert (not _menhir_env._menhir_error);
                let _tok = _menhir_env._menhir_token in
                (match _tok with
                | NEWLINE _v ->
                    _menhir_run7 _menhir_env (Obj.magic _menhir_stack) MenhirState4 _v
                | OFF _v ->
                    _menhir_run6 _menhir_env (Obj.magic _menhir_stack) MenhirState4 _v
                | ON _v ->
                    _menhir_run5 _menhir_env (Obj.magic _menhir_stack) MenhirState4 _v
                | _ ->
                    assert (not _menhir_env._menhir_error);
                    _menhir_env._menhir_error <- true;
                    _menhir_errorcase _menhir_env (Obj.magic _menhir_stack) MenhirState4)
            | _ ->
                assert (not _menhir_env._menhir_error);
                _menhir_env._menhir_error <- true;
                let _menhir_stack = Obj.magic _menhir_stack in
                raise _eRR)
        | _ ->
            assert (not _menhir_env._menhir_error);
            _menhir_env._menhir_error <- true;
            let _menhir_stack = Obj.magic _menhir_stack in
            raise _eRR)
    | _ ->
        assert (not _menhir_env._menhir_error);
        _menhir_env._menhir_error <- true;
        let _menhir_stack = Obj.magic _menhir_stack in
        raise _eRR)

# 219 "/Users/ricardo/.opam/system/lib/menhir/standard.mly"
  


# 324 "parser.ml"
