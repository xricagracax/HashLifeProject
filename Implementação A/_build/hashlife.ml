	(*Abrir a biblioteca da tabela de hash*)
open Hashtbl;;
(*Abrir o módulo das listas*)
open List;;
(*Abrir o módulo de Hashconsing criado por Jean-Christophe Filliâtre*)
open Hashcons;;
(*Abrir o módulo dos Arrays*)
open Array;;
(*Abrir o módulo de Inteiros grandes*)
open Big_int_Z;;
(*tmp*)
open Printf;;

(*Iniciar o gerador de números aleatórios*)
Random.self_init ();;

(*Definição estrutural da árvore*)
type tree = tree_node hash_consed 
	and tree_node =
	| Cell of bool * bool * bool * bool
	(*MacroCell(_,_,_,_),dimensao,numero de celulas vivas, resultados*)
	| MacroCell of ((tree * tree * tree * tree) * int * Z.t * result ref)
	and result = 
	| NIL
	| Result of tree_node hash_consed 

(*Criar um módulo para a estrutura definida acima*)
module Tree_node = struct
	type t = tree_node
	let equal t1 t2 = match t1, t2 with 
	| MacroCell((nw1,ne1,sw1,se1),_,_,_), MacroCell((nw2,ne2,sw2,se2),_,_,_) -> nw1 == nw2 && ne1 == ne2 && sw1 == sw2 && se1 == se2
	| Cell(nw1,ne1,sw1,se1), Cell(nw2,ne2,sw2,se2) -> nw1 = nw2 && ne1 = ne2 && sw1 = sw2 && se1 = se2
	| _ -> false
	let hash = function 
	  | Cell(nw,ne,sw,se) -> begin
		let int_of_boolean = function
		| true  -> 1
		| false -> 0 in 
		let rec hash_cell l acc i = 
		match l with 
		| []   -> acc
		| x::y -> hash_cell y (acc + (1 lsl i)*(int_of_boolean x)) (i+1) in 
		hash_cell [nw;ne;sw;se] 0 0
	end
	| MacroCell((nw,ne,sw,se),_,_,_) ->  abs (19 * (19 * (19 * nw.hkey + ne.hkey) + sw.hkey) + se.hkey)
end

(*Criar um módulo a partir de um functor*)
module HashTree = Hashcons.Make(Tree_node)

(*Inicialização da tabela de hash onde irão ser guardadas as macro-células*)
let size = (1 lsl 19) - 1
let ht = HashTree.create (size)

(*Buscar número de células vivas da macro-célula*)
let number_cells m = 
	match m.node with 
	| Cell (a,b,c,d) -> Array.fold_left (fun acc x -> if x then (Big_int_Z.add_big_int Z.one acc) else acc) (Z.zero) [|a;b;c;d|]
	| MacroCell ((nw,ne,sw,se),_,n,_) -> n

(*Dimensão de uma macro-célula*)
let dimension m = 
	match m.node with 
	| Cell _ -> 1
	| MacroCell ((_,_,_,_),d,_,_) -> d 

(*Resultado de uma macro-célula*)
let result m = 
	match m.node with 
	| Cell _ -> None
	| MacroCell ((_,_,_,_),_,_,r) -> Some (!r)

(*Construtores inteligentes para inserção das macro-células*)
let cell (nw,ne,sw,se) = HashTree.hashcons ht (Cell (nw,ne,sw,se))
let macrocell (nw,ne,sw,se) = 
	let dim = dimension nw in 
	let n = Array.fold_left (fun acc x -> Big_int_Z.add_big_int acc (number_cells x)) (Z.zero) [|nw;ne;sw;se|] in 
	HashTree.hashcons ht (MacroCell ((nw,ne,sw,se),dim+1,n,ref NIL))

(*Árvore aleatória*)
let rec random_tree n = 
	match n with 
	| 1 -> let b () = Random.bool () in cell (b(),b(),b(),b())
	| _ -> let p () = random_tree (n-1) in macrocell (p(),p(),p(),p())

(*Associar resultado à macrocélula*)
let insert_result t1 t2 = 
	match t1.node with 
	| MacroCell(a,b,c,d) -> (
		match !d with 
		| NIL -> d := Result(t2); Some(t2)
		| Result r -> raise (Failure "Só macro-células de dimensão maior que 1 é que têm resultado")
	);
	| Cell(_) -> raise (Failure "Só macro-células de dimensão maior que 1 é que têm resultado")

(*Buscar o resultado*)
let get_result_with_steps t1 = 
	match t1.node with 
	| MacroCell(a,b,c,d) -> (
		match !d with 
		| NIL -> None
		| Result r -> Some(r)
	)
	| Cell(_) -> raise (Failure "Só macro-células de dimensão maior que 1 é que têm resultado")

(*Macro-célula vazia*)
let blank_macro_cell = 
	(*Hashtable de macrocélulas vazias*)
	let ht_blank = Hashtbl.create 13 in 
	let rec aux n = 
	(
		match n with 
		| 1 -> cell (false,false,false,false)
		| _ -> 
		try 
			Hashtbl.find ht_blank n 
		with Not_found -> 
			let p = aux (n-1) in
			let m = macrocell (p,p,p,p) in 
			Hashtbl.add ht_blank n m; 
			macrocell (p,p,p,p)
	) in aux

(*Macro-célula vazia*)
let black_macrocell = 
	(*Hashtable de macrocélulas vazias*)
	let ht_black = Hashtbl.create 13 in 
	let rec aux n = 
	(
		match n with 
		| 1 -> cell (true,true,true,true)
		| _ -> 
		try 
			Hashtbl.find ht_black n 
		with Not_found -> 
			let p = aux (n-1) in
			let m = macrocell (p,p,p,p) in 
			Hashtbl.add ht_black n m; 
			macrocell (p,p,p,p)
	) in aux

(*Ver se macro-célula se encontra vazia*)
let rec empty_macrocell t = 
	match t with 
	| Cell (nw,ne,sw,se) -> not (nw || ne || sw || se)
	| MacroCell ((nw,ne,sw,se),d,_,_) -> 
		let blank = blank_macro_cell (d-1)
		in List.for_all (fun x -> x.tag == blank.tag) [nw;ne;sw;se]

(*Ver se macro-célula se encontra cheia*)
let rec full_macrocell t = 
	match t with 
	| Cell (nw,ne,sw,se) -> (nw && ne && sw && se)
	| MacroCell ((nw,ne,sw,se),d,_,_) -> 
		let black = black_macrocell (d-1)
		in List.for_all (fun x -> x.tag == black.tag) [nw;ne;sw;se]

(*Buscar o centro da célula*)
let center_of_cell t = 
	match t.node with 
	| Cell (a,b,c,d) -> raise (Failure "É uma célula de dimensão 1!")
	| MacroCell ((nw,ne,sw,se),d,_,_) -> (
	match nw.node, ne.node, sw.node, se.node with 
	| MacroCell ((nw1,ne1,sw1,se1),_,_,_),MacroCell ((nw2,ne2,sw2,se2),_,_,_),MacroCell ((nw3,ne3,sw3,se3),_,_,_),MacroCell ((nw4,ne4,sw4,se4),_,_,_) ->
		macrocell(se1,sw2,ne3,nw4) 
	| Cell (nw1,ne1,sw1,se1),Cell (nw2,ne2,sw2,se2),Cell (nw3,ne3,sw3,se3), Cell (nw4,ne4,sw4,se4) -> cell(se1,sw2,ne3,nw4)
	| _ -> raise (Failure "Ocorreu um erro")
	)

(*Buscar os stats*)
let stats () = 
	(HashTree.stats ht),HashTree.getTheSize ht

(*Devolve uma macro-célula de dimensão n-1 se possivel*)
let rec get_lower t = 
	match t.node with 
	| Cell (a,b,c,d) -> t 
	| MacroCell ((nw,ne,sw,se),b,c,d) -> (
		let aux = ref t in 
		if (List.fold_left (fun acc m -> if not (empty_macrocell m.node) then (aux := m; acc) else 1+acc) 0 [nw;ne;sw;se]) = 3 then (
			get_lower !aux
		)
		else(
			match nw.node, ne.node, sw.node, se.node with 
			| MacroCell ((nw1,ne1,sw1,se1),_,_,_),MacroCell ((nw2,ne2,sw2,se2),_,_,_),MacroCell ((nw3,ne3,sw3,se3),_,_,_),MacroCell ((nw4,ne4,sw4,se4),_,_,_)
			-> (
				let f l  = List.exists (fun m -> not (empty_macrocell m.node)) l in 
				if not (f [nw1;ne1;nw2;ne2; 
									 sw1;se1; 
									 nw3;ne3;
									 sw3;se3;sw4;se4]) then (
					get_lower (macrocell(sw2,se2,nw4,ne4))
				)
				else(
				if not (f [nw1;ne1;nw2;ne2; 
														sw2;se2; 
														nw4;ne4;
										sw3;se3;sw4;se4]) then (
					get_lower (macrocell(sw1,se1,nw3,ne3))
					)
					else(
				if not (f [nw1;ne1;nw2;ne2; 
										sw1;se1;sw2;se2; 
										nw3        ;ne4;
										sw3        ;se4]) then (
					get_lower (macrocell(ne3,nw4,se3,sw4))
						)
						else(
				if not (f  [nw1        ;ne2; 
										sw1        ;se2; 
										nw3;ne3;nw4;ne4;
										sw3;se3;sw4;se4]) then (
					get_lower (macrocell(ne1,nw2,se1,sw2))
							)
							else(
								t
							)
						)
					)
				)
			)
			| Cell (a1,b1,c1,d1),Cell (a2,b2,c2,d2),Cell (a3,b3,c3,d3),Cell (a4,b4,c4,d4) -> 
				if not (a1 || b1 || a2 || b2 || d2 || b4 || d4 || c4 || d3 || c3 || a3 || a3) then (
					get_lower (cell (d1,c2,b3,a4))
				) else t
			| _ -> t
		)
	)

(*Reduzir uma macro-célula*)
let rec reduce_macrocell t = 
	match t.node with 
	| Cell _ -> t
	| MacroCell ((nw,ne,sw,se),_,_,_) -> 
	match nw.node, ne.node, sw.node, se.node with 
	| MacroCell((nw1,ne1,sw1,se1),_,_,_), MacroCell((nw2,ne2,sw2,se2),_,_,_), 
		MacroCell((nw3,ne3,sw3,se3),_,_,_), MacroCell((nw4,ne4,sw4,se4),_,_,_) ->
		if List.for_all empty_macrocell 
			[nw1.node;ne1.node;nw2.node;ne2.node; 
				sw1.node;                  se2.node; 
				nw3.node;                  ne4.node;
				sw3.node;se3.node;sw4.node;se4.node]
		then 
			reduce_macrocell (macrocell (se1,sw2,ne3,nw4))
		else(
			get_lower t
		)
	| _ -> t

(*Aumentar a dimensão da macro-célula por 1*)
let rec extend_macrocell t = 
	match t.node with 
	| Cell (nw,ne,sw,se) -> 
		macrocell (
			cell(false,false,false,nw),
			cell(false,false,ne,false),
			cell(false,sw,false,false),
			cell(se,false,false,false) 
		)
	| MacroCell ((nw,ne,sw,se),d,_,_) -> 
		let black_cell = blank_macro_cell (d-1) in 
		macrocell (
			macrocell(black_cell,black_cell,black_cell,nw),
			macrocell(black_cell,black_cell,ne,black_cell),
			macrocell(black_cell,sw,black_cell,black_cell),
			macrocell(se,black_cell,black_cell,black_cell) 
		)

(*Resultado base para uma célula, dimensão = 2*)
let base_result t = 
	match t with 
	| MacroCell((nw,ne,sw,se),_,_,_) -> (
		let life (a, b, c, d, e, f, g, h, i) = 
		match (List.fold_left (fun acc e -> match e with false -> acc | _ -> acc + 1 ) 0 [a;b;c;d;f;g;h;i]) with 
		| 2 -> e
		| 3 -> true 
		| _ -> false in 
		match nw.node, ne.node, sw.node, se.node with 
		| Cell(nw1,ne1,sw1,se1), Cell(nw2,ne2,sw2,se2), Cell(nw3,ne3,sw3,se3), Cell(nw4,ne4,sw4,se4) -> begin 
			let new_nw, new_ne = life(nw1,ne1,nw2,sw1,se1,sw2,nw3,ne3,nw4), life(ne1,nw2,ne2,se1,sw2,se2,ne3,nw4,ne4) in 
			let new_sw, new_se = life(sw1,se1,sw2,nw3,ne3,nw4,sw3,se3,sw4), life(se1,sw2,se2,ne3,nw4,ne4,se3,sw4,se4) in 
			cell(new_nw, new_ne, new_sw, new_se)
			end
		| _ -> raise (Failure "Não é uma célula de dimensão 2!")
	)
	| _ -> raise (Failure "Não é uma célula de dimensão 2!")

(*Estrutura big endian binary*)
type big_endian_binary = int * bool list

let cons_digit digit li =
	if digit = false && li = [] then []
	else digit :: li

let bebin_of_int n =
	(* iterates over the binary digits of n *)
	let two_in_big = Big_int_Z.big_int_of_int 2 in  
	let rec loop rank acc n =
	let acc, n = cons_digit (Big_int_Z.gt_big_int (Big_int_Z.mod_big_int n two_in_big) Z.zero) acc, Big_int_Z.div_big_int n two_in_big in
	if n = Z.zero then (rank, acc)
	else loop (rank + 1) acc n
	in loop 0 [] n

(*Converte um numero em um big endian binary com rank*)
let bebin_of_int_with_rank end_rank n =
  let rec loop (rank, li) =
	if rank < end_rank then loop (rank + 1, false :: li)
	else begin
	if not (rank = end_rank) then
	failwith "OVERFLOW";
	(rank, li)
	end in
	loop (bebin_of_int n)
	
(*Faz o inverse do bebin_of_int_with_rank*)
let int_of_bebin (rank, bebin) =
  let rec loop rank = function
	| [] -> Z.zero
	| b :: rest ->
		Big_int_Z.add_big_int (loop (rank - 1) rest) (if b then Big_int_Z.shift_left_big_int Z.one rank else Z.zero)
  in loop rank bebin

(*Remove os zeros finais*)
let remove_trailing_zeroes li =
	List.fold_right cons_digit li []

(*Divide o trabalho entre as células*)
let split_work = function
	| (0, _) -> None
	| (r, work) ->
	let wa, wb = match work with
	| [] -> [],[]
	| true::rest ->
		if remove_trailing_zeroes rest <> [] then
			invalid_arg "demasiado grande"
		else ([true], [true])
	| false::true::rest -> false::rest, [true]
	| false::false::rest -> [], false::rest
	| false::[] -> [], []
	in Some ((r - 1, wa), (r - 1, wb))
	
(*Inductive result with steps*)
let inductive_result f (ai,bi,ci,di) steps_a steps_b = 
	match ai.node, bi.node, ci.node, di.node with
	MacroCell((a1,a2,a3,a4),_,_,_),
	MacroCell((b1,b2,b3,b4),_,_,_),
	MacroCell((c1,c2,c3,c4),_,_,_),
	MacroCell((d1,d2,d3,d4),_,_,_) -> 
	let r1 = f ai steps_a in 
	let r2 = f (macrocell (a2,b1,a4,b3)) steps_a in
	let r3 = f bi steps_a in 
	let r4 = f (macrocell (a3,a4,c1,c2)) steps_a in
	let r5 = f (macrocell (a4,b3,c2,d1)) steps_a in
	let r6 = f (macrocell (b3,b4,d1,d2)) steps_a in
	let r7 = f ci steps_a in 
	let r8 = f (macrocell (c2,d1,c4,d3)) steps_a in
	let r9 = f di steps_a in 
	let j1 = f (macrocell (r1,r2,r4,r5)) steps_b in
	let j2 = f (macrocell (r2,r3,r5,r6)) steps_b in
	let j3 = f (macrocell (r4,r5,r7,r8)) steps_b in
	let j4 = f (macrocell (r5,r6,r8,r9)) steps_b in
	let result = macrocell (j1,j2,j3,j4) in 
	result
	| _ -> raise (Failure "Célula heterogénea")
(*Resultado, para todos os tamanhos*)
let tree_result_generalized t num_steps =
	(* let steps = bebin_of_int_with_rank (dimension t - 2) num_steps in *)
	let rec get_result t steps = 
	(* let aux_steps = int_of_bebin steps in  *)
	if (Big_int_Z.eq_big_int Z.zero steps) then (
		center_of_cell t
	)
	else(
		if dimension t = 2 then (
			match t.node with 
			| MacroCell((a1,b1,c1,d1),_,_,_) -> (
				let r = get_result_with_steps t in 
				if r = None then (
					let result = base_result (macrocell (a1,b1,c1,d1)).node in 
					insert_result t result;
					result
				)else(
					let Some(a1) = r in 
					a1
				)
			)
		)else(
		let largura = (Big_int_Z.shift_left_big_int Z.one ((dimension t) - 2)) in 
		if Big_int_Z.gt_big_int steps largura then (
			raise (Failure "Too big")
		);
		let b = Big_int_Z.shift_right_big_int largura 1 in 
		if Big_int_Z.eq_big_int steps largura then (
			let r = get_result_with_steps t in 
			if  r = None then (
				match t.node with 
				| MacroCell((a1,b1,c1,d1),_,_,_) -> (
					let result = inductive_result get_result (a1,b1,c1,d1) b b in 
					insert_result t result;
					result
				)
				| _ -> raise (Failure "Célula de nivel menor que dois")
			)else(
				let Some(a1) = r in 
				a1
			)
		)
		else (
			if Big_int_Z.le_big_int steps b then (
				match t.node with 
				| MacroCell((a1,b1,c1,d1),_,_,_) -> (
					let result = inductive_result get_result (a1,b1,c1,d1) Z.zero steps in 
					result
				)
			)
			else(
				match t.node with 
				| MacroCell((a1,b1,c1,d1),_,_,_) -> (
					let result = inductive_result get_result (a1,b1,c1,d1) b (Big_int_Z.sub_big_int steps b) in 
					result
				)
			)
		)
	)
	)

	in get_result t num_steps

(*Ir mais para cima*)
let tree_result_go_up t steps = 
	let t_aux = ref t in 
	if dimension (t) < 2 then (
		raise (Failure "You need to load a configuration!")
	);
	while Big_int_Z.lt_big_int (Big_int_Z.shift_left_big_int Z.one (dimension (!t_aux) - 2)) steps do 
		t_aux := extend_macrocell (!t_aux);
	done;
	!t_aux
;;

(*Buscar resultado n passos no futuro*)
let tree_result_generalized_up t steps = 

	let t_aux = ref t in 
	if dimension (t) < 2 then (
		raise (Failure "You need to load a configuration!.")
	);(* 
	while Big_int_Z.lt_big_int (Big_int_Z.shift_left_big_int Z.one (dimension (!t_aux) - 3)) steps do 
		t_aux := extend_macrocell (!t_aux);
	done; *)


	while (Big_int_Z.lt_big_int (Big_int_Z.sub_big_int (Big_int_Z.shift_left_big_int Z.one (dimension (!t_aux))) (Big_int_Z.shift_left_big_int Z.one (dimension (t) - 3))) steps) do 
		t_aux := extend_macrocell (!t_aux);
	done;

	tree_result_generalized (!t_aux) steps

(*Transforma a matriz em árvore*)
let matrix_to_tree m = 
	let size = Array.length m in 
		let rec aux mi ni si = 
		match si with 
		| 2 -> cell(m.(mi).(ni),m.(mi).(ni+1),m.(mi+1).(ni),m.(mi+1).(ni+1))
		| _ -> (
		let si = si / 2 in 
		let aux1 = aux (mi) (ni) (si) in 
		let aux2 = aux (mi+si) (ni) (si) in 
		let aux3 = aux (mi) (ni+si) (si) in 
		let aux4 = aux (mi+si) (ni+si) (si) in 
		macrocell (aux1,aux3,aux2,aux4);
	)
	in aux 0 0 (size)

(*Transforma a matriz em árvore*)
let tree_to_matrix t = 
	if dimension t > 15 then (
		raise (Failure "Demasiado grande!")
	)
	else(
		let size = dimension t in 
		let side = (1 lsl size) in 
		let myArray = Array.init side (fun _ -> Array.init side (fun _ -> false)) in 
		let rec aux ti a b size = 
			match ti.node with 
			| Cell(nw,ne,sw,se) -> (
					myArray.(a).(b) <- nw;
					myArray.(a+1).(b) <- sw;
					myArray.(a).(b+1) <- ne;
					myArray.(a+1).(b+1) <- se;
			)
			| MacroCell((nw,ne,sw,se),_,_,_) -> (
				aux (nw) (a) (b) (size/2);
				aux (ne) (a+size/2) (b) (size/2);
				aux (sw) (a) (b+size/2) (size/2);
				aux (se) (a+size/2) (b+size/2) (size/2);
			)
			in aux t 0 0 size; myArray
		)
