(*Biblioteca para mostrar os gráficos*)
open Graphics;;
open Printf;;
open Hashlife;;
open Unix;;
open Scanf;;
open Array;;

(*Desenhar árvore*)
let draw_tree t = 

	clear_graph ();	
	let x = size_x () in 
	let y = size_y () in 

	let draw_cell bl n a b = 
	match bl with 
	| true  -> set_color black; fill_rect (a) (b) (x/n) (y/n)
	| _     -> set_color white; fill_rect (a) (b) (x/n) (y/n) in
	
	let rec draw_tree_aux ti n a b = 
	let new_n = n*2 in 
	match ti with 
	| Cell(nw,ne,sw,se) -> begin
		draw_cell nw (new_n) (    a    ) (b+y/new_n);
		draw_cell ne (new_n) (a+x/new_n) (b+y/new_n);
		draw_cell sw (new_n) (    a    ) (    b    );
		draw_cell se (new_n) (a+x/new_n) (    b    );
		end
	| MacroCell((nw,ne,sw,se),_,_) -> begin
		if empty_macrocell ti then 
			()
		else
			draw_tree_aux nw.node (new_n) (    a    ) (b+y/new_n);
			draw_tree_aux ne.node (new_n) (a+x/new_n) (b+y/new_n);
			draw_tree_aux sw.node (new_n) (    a    ) (    b    );
			draw_tree_aux se.node (new_n) (a+x/new_n) (    b    );
		end
	in draw_tree_aux (t) 1 0 0

(*Desenhar árvore*)
let draw_matrix m = 

	clear_graph ();	
	let x = size_x () in 
	let y = size_y () in 
	let size = Array.length m in 
	
	let draw_cell bl i j = 
	match bl with 
	| true  -> set_color black; fill_rect ((j)*(x/size)) ((size-1-i)*(y/size)) (x/size) (y/size)
	| _     -> set_color white; fill_rect ((j)*(x/size)) ((size-1-i)*(y/size)) (x/size) (y/size) in
	
	for i=0 to (size-1) do 
		for j=0 to (size-1) do 
			draw_cell m.(i).(j) i j
		done;
	done

let print_matrix m = 
	let size = Array.length m in 
	for i=0 to (size-1) do 
		for j=0 to (size-1) do
			if m.(i).(j) then 
				printf "X"
			else printf " "
		done; print_newline ();
	done

(*Main program 1*)
let main_1() = 
	let tree_aux = (pattern_tree 11) in
	let result = (tree_result tree_aux) in 
	printf "DONE!";
	print_newline();
	exit(0)

(*Eventos gráficos*)
let rec interactive key = 

	let () = 
	match key with 
	|'1'   -> main_1(); 
	|'2'   -> ();
	|'3'   -> ();
	|'\b'  -> exit(1);
	|_     -> printf "NONE SELECTED"; print_newline (); in 

	interactive (wait_next_event [Key_pressed]).key

(* Ponto de entrada do programa principal *)
let _ = 
	main_1();
	interactive (wait_next_event [Key_pressed]).key;