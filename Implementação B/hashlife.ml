(*Abrir a biblioteca da tabela de hash*)
open Hashtbl;;
(*Abrir o módulo das listas*)
open List;;
(*Abrir o módulo de Hashconsing criado por Jean-Christophe Filliâtre*)
open Hashcons;;
(*Abrir o módulo dos Arrays*)
open Array;;
(*tmp*)
open Printf;;

(*Iniciar o gerador de números aleatórios*)
Random.self_init ();;

(*Definição estrutural da árvore*)
type tree = tree_node hash_consed 
	and tree_node =
	| Cell of bool * bool * bool * bool
	| MacroCell of ((tree * tree * tree * tree) * int * result ref)
	and result = 
	| NIL
	| Result of tree

(*Criar um módulo para a estrutura definida acima*)
module Tree_node = struct
	type t = tree_node
	let equal t1 t2 = match t1, t2 with 
	| MacroCell((nw1,ne1,sw1,se1),_,_), MacroCell((nw2,ne2,sw2,se2),_,_) -> nw1 == nw2 && ne1 == ne2 && sw1 == sw2 && se1 == se2
	| Cell(nw1,ne1,sw1,se1), Cell(nw2,ne2,sw2,se2) -> nw1 = nw2 && ne1 = ne2 && sw1 = sw2 && se1 = se2
	| _ -> false
	let hash = function 
	  | Cell(nw,ne,sw,se) -> begin
		let int_of_boolean = function
		| true  -> 1
		| false -> 0 in 
		let rec hash_cell l acc i = 
		match l with 
		| []   -> acc
		| x::y -> hash_cell y (acc + (1 lsl i)*(int_of_boolean x)) (i+1) in 
		hash_cell [nw;ne;sw;se] 0 0
	end
	| MacroCell((nw,ne,sw,se),_,_) -> Hashtbl.hash (nw.hkey,ne.hkey,sw.hkey,se.hkey)
end

(*Criar um módulo a partir de um functor*)
module HashTree = Hashcons.Make(Tree_node)

(*Inicialização da tabela de hash onde irão ser guardadas as macro-células*)
let size = (1 lsl 19) - 1
let ht = HashTree.create (size)

(*Construtores inteligentes para inserção das macro-células*)
let cell (nw,ne,sw,se) = HashTree.hashcons ht (Cell (nw,ne,sw,se))
let macrocell (nw,ne,sw,se) = 
	let dim = begin
		match nw.node with 
		|MacroCell(_,b,_) -> b
		|Cell(_) -> 1 
	end in 
	HashTree.hashcons ht (MacroCell ((nw,ne,sw,se),dim+1,ref NIL))

(*Associar resultado à macrocélula*)
let insert_result t1 t2 = 
	match t1.node with 
	| MacroCell(a,b,c) -> c := Result t2;
	| Cell(_) -> raise (Failure "Só macro-células de dimensão maior que 1 é que têm resultado")

(*Dimensão de uma macro-célula*)
let dimension t = begin
	match t.node with 
	| Cell _ -> 1
	| MacroCell(_,d,_) -> d 
end

(*Hashtable de macrocélulas vazias*)
let ht_blank = Hashtbl.create 7;;

(*Macro-célula vazia*)
let rec blank_macro_cell n = 
	match n with 
	| 1 -> cell (false,false,false,false)
	| _ -> 
		try 
			Hashtbl.find ht_blank n 
		with Not_found -> 
			let p = blank_macro_cell (n-1) in
			let m = macrocell (p,p,p,p) in 
			Hashtbl.add ht_blank n m; 
			macrocell (p,p,p,p)

(*Ver se macro-célula se encontra vazia*)
let rec empty_macrocell t = 
	match t with 
	| Cell (nw,ne,sw,se) -> not (nw || ne || sw || se)
	| MacroCell ((nw,ne,sw,se),d,_) -> 
		let blank = blank_macro_cell (d-1)
		in nw.tag == blank.tag && ne.tag == blank.tag && sw.tag == blank.tag && se.tag == blank.tag

(*Reduzir uma macro-célula*)
let rec reduce_macrocell t = 
		match t.node with 
		| Cell _ -> t
		| MacroCell ((nw,ne,sw,se),_,_) -> 
		match nw.node, ne.node, sw.node, se.node with 
		| MacroCell((nw1,ne1,sw1,se1),_,_), MacroCell((nw2,ne2,sw2,se2),_,_), 
			MacroCell((nw3,ne3,sw3,se3),_,_), MacroCell((nw4,ne4,sw4,se4),_,_) ->
			if List.for_all empty_macrocell 
				[nw1.node;ne1.node;nw2.node;ne2.node; 
				 sw1.node;                  se2.node; 
				 nw3.node;                  ne4.node;
				 sw3.node;se3.node;sw4.node;se4.node]
			then 
				reduce_macrocell (macrocell (se1,sw2,ne3,nw4))
			else
				t
		| _ -> t

let rec extend_macrocell t = 
	match t.node with 
	| Cell (nw,ne,sw,se) -> 
		macrocell (
			cell(false,false,false,nw),
			cell(false,false,ne,false),
			cell(false,sw,false,false),
			cell(se,false,false,false) 
		)
	| MacroCell ((nw,ne,sw,se),d,_) -> 
		let black_cell = blank_macro_cell (d-1) in 
		macrocell (
			macrocell(black_cell,black_cell,black_cell,nw),
			macrocell(black_cell,black_cell,ne,black_cell),
			macrocell(black_cell,sw,black_cell,black_cell),
			macrocell(se,black_cell,black_cell,black_cell) 
		)

(*Resultado base para uma célula, dimensão = 2*)
let base_result nw ne sw se = 
	let life (a, b, c, d, e, f, g, h, i) = 
	match (List.fold_left (fun acc e -> match e with false -> acc | _ -> acc + 1 ) 0 [a;b;c;d;f;g;h;i]) with 
	| 2 -> e
	| 3 -> true 
	| _ -> false in 
	match nw, ne, sw, se with 
	| Cell(nw1,ne1,sw1,se1), Cell(nw2,ne2,sw2,se2), Cell(nw3,ne3,sw3,se3), Cell(nw4,ne4,sw4,se4) -> begin 
			let new_nw, new_ne = life(nw1,ne1,nw2,sw1,se1,sw2,nw3,ne3,nw4), life(ne1,nw2,ne2,se1,sw2,se2,ne3,nw4,ne4) in 
			let new_sw, new_se = life(sw1,se1,sw2,nw3,ne3,nw4,sw3,se3,sw4), life(se1,sw2,se2,ne3,nw4,ne4,se3,sw4,se4) in 
				cell(new_nw, new_ne, new_sw, new_se)
		end
	| _ -> raise (Failure "Não é uma célula de dimensão 2!")

let inductive_result f (ai,bi,ci,di) = 
	match ai.node, bi.node, ci.node, di.node with
	MacroCell((a1,a2,a3,a4),_,_),
	MacroCell((b1,b2,b3,b4),_,_),
	MacroCell((c1,c2,c3,c4),_,_),
	MacroCell((d1,d2,d3,d4),_,_) -> 
		let r1 = f ai in 
		let r2 = f (macrocell (a2,b1,a4,b3)) in
		let r3 = f bi in 
		let r4 = f (macrocell (a3,a4,c1,c2)) in
		let r5 = f (macrocell (a4,b3,c2,d1)) in
		let r6 = f (macrocell (b3,b4,d1,d2)) in
		let r7 = f ci in 
		let r8 = f (macrocell (c2,d1,c4,d3)) in
		let r9 = f di in 
		let j1 = f (macrocell (r1,r2,r4,r5)) in
		let j2 = f (macrocell (r2,r3,r5,r6)) in
		let j3 = f (macrocell (r4,r5,r7,r8)) in
		let j4 = f (macrocell (r5,r6,r8,r9)) in
		let result = macrocell (j1,j2,j3,j4) in 
		result
	| _ -> raise (Failure "Célula heterogénea")

(*Resultado generalizado, para qualquer tamanho*)
let rec tree_result t = 
	match t.node with 
	|Cell _ -> raise (Failure "Células de dimensão 1 não têm resultado!")
	|MacroCell (a,b,c) -> begin
		match !c with 
		| NIL -> (
			match dimension (t) with
			| 2 -> let (ai,bi,ci,di) = (a) in
						  let result = base_result ai.node bi.node ci.node di.node in 
							insert_result t result;
							result
			| _ -> let result = inductive_result tree_result a in 
							insert_result t result;
						  result
		);
		| Result r -> r
	end

(*Converter árvore para matriz*)
let tree_to_matrix t = 

	let d = dimension t in 

	let matrix = init (1 lsl d) (fun _ -> init (1 lsl d) (fun _ -> false)) in 
	let rec tree_to_matrix_aux t a b size = 
		match t.node with 
		| Cell(nw,ne,sw,se) -> begin
			matrix.( a ).( b ) <- nw;
			matrix.( a ).(b+1) <- ne;
			matrix.(a+1).( b ) <- sw;
			matrix.(a+1).(b+1) <- se;
		end
		| MacroCell((nw,ne,sw,se),_,_) -> begin
			tree_to_matrix_aux nw (  a   ) (   b  ) (size/2);
			tree_to_matrix_aux ne (  a   ) (b+size) (size/2);
			tree_to_matrix_aux sw (a+size) (   b  ) (size/2);
			tree_to_matrix_aux se (a+size) (b+size) (size/2);
		end
	in tree_to_matrix_aux t 0 0 (1 lsl (d-1));
	matrix

(*Exemplo*)
let rec pattern_tree n = 
	match n with 
	| 1 -> let b () = Random.bool () in cell (b(),b(),b(),b())
	(* | 1 -> cell (false,true,true,false) *)
	(* | _ -> let p = pattern_tree (n-1) in macrocell (p,p,p,p) *)
	| _ -> let p () = pattern_tree (n-1) in macrocell (p(),p(),p(),p())